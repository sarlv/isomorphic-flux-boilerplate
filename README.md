# For run this project you need: #
--------------------

## Dependencies ##
----------------

1. Node 4.4.3.
2. NPM 3.8.6.
3. Mongo 3.2.5
## To Install ##
----------------

1. Install node (https://nodejs.org/en/)
2. Clone the repo and cd into the directly
3. Run `npm install`
4. Run `sudo mongod`
5. If you open this app first time, you need to run `./node_modules/bower/bin/bower install`
6. Run `npm start`
7. Open `http://localhost:3000/` in your browser
8. For runnign tests `npm test`
