require('babel-core/register')({
    'presets': ['es2015'],
    ignore: /node_modules/
});

const app = require('./config/lib/app');
const server = app.start();
