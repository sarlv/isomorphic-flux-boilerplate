import config from '../config';
import db from './mongoose';
import path from 'path';
import glob from 'glob';
import express from 'express';
import bodyParser from 'body-parser';
import flash from 'express-flash';

import webpack from 'webpack';
import webpackMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import webpackConfig from '../../webpack.config.js';

const port = process.env.PORT || config.default.port;
const isDeveloping = process.env.NODE_ENV !== 'production';

const initWebPack = (app) => {

    if (isDeveloping) {
        var compiler = webpack(webpackConfig);
        var middleware = webpackMiddleware(compiler, {
            publicPath: webpackConfig.output.publicPath,
            contentBase: 'src',
            stats: {
                colors: true,
                hash: false,
                timings: true,
                chunks: false,
                chunkModules: false,
                modules: false
            }
        });

        app.use(middleware);
        app.use(webpackHotMiddleware(compiler));
    }
}

const initMiddleware = (app) => {
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json({}));
    app.use(bodyParser.raw({
        type: 'application/vnd.custom-type'
    }));
    app.use(bodyParser.text({
        type: 'text/html'
    }));

    app.use(express.static(`${__dirname}/../../public`));
    app.use(express.static(`${__dirname}/../../assets`));

    app.use(require('cookie-parser')());
    app.use(require('express-session')({
        secret: config.default.session,
        resave: true,
        saveUninitialized: true
    }));
    app.use(flash());
    app.set('view engine', 'ejs');
    app.set('env', (process.env.NODE_ENV || 'development'));
}

const initModule = (app, db) => {
    config.assets.server.modules.forEach((file) => {
        require(`${__dirname}/../../${file}`)(app, db);
    });
};

const initRoutes = (app, db) => {
    config.assets.server.routes.forEach((configPath) => {
        glob(configPath, (err, files) => {
            files.forEach((file) => {
                require(`${__dirname}/../../${file}`)(app, db);
            });
        });
    });
};

const initPublicDirectories = (app) => {
    config.assets.client.public.forEach((dir) => {
        app.use(express.static(dir));
    });
};

export function init(callback) {
    const app = express();
    const EventEmitter = require('events').EventEmitter;

    app.eventEmitter = new EventEmitter();

    app.listen(port, () => {
        console.log(`Node app is running on port ${port}`);
    });

    initWebPack(app);
    initModule(app);
    initRoutes(app);
    initMiddleware(app);
    initPublicDirectories(app);
}
