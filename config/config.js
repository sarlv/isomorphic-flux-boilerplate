import assets from './assets/default';
import envdef from './env/default';

export default (function() {
    return {
        assets: assets,
        default: envdef
    }
}());
