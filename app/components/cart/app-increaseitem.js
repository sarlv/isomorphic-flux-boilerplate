import React from 'react';
import AppActions from '../../actions/app-actions';

const IncreaseItem = React.createClass({
  handler: function(){
    AppActions.increaseItem(this.props.index)
  },
  render:function(){
    return <button onClick={this.handler}>+</button>
  }
});

module.exports = IncreaseItem;
