import React from 'react';
import AppActions from '../../actions/app-actions';

const DecreaseItem = React.createClass({
  handler: function(){
    AppActions.decreaseItem(this.props.index)
  },
  render:function(){
    return <button onClick={this.handler}>-</button>
  }
});

module.exports = DecreaseItem;
