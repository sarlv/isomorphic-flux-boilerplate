import React from 'react';
import AppActions from '../../actions/app-actions';

const RemoveFromCart = React.createClass({
  handler: function(){
    AppActions.removeItem(this.props.index)
  },
  render:function(){
    return <button onClick={this.handler}>x</button>
  }
});

module.exports = RemoveFromCart;
