import React from 'react';
import AppStore from '../../stores/app-store.js';
import AddToCart from './app-addtocart.js'
import StoreWatchMixin from '../../mixins/StoreWatchMixin';
import CatalogItem from '../catalog/app-catalogitem';


function getCatalog(){
  return {items: AppStore.getCatalog()}
}

const Catalog = React.createClass({
  mixins:[StoreWatchMixin(getCatalog)],
  render:function(){
    let items = this.state.items.map(function(item){
      return <CatalogItem key={item.id} item={item} />

    })
    return (
      <div className="row">
        {items}
      </div>
    )
  }
});

module.exports = Catalog
