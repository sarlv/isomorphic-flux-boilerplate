import React from 'react';
import Catalog from './catalog/app-catalog';
import Cart from './cart/app-cart';
import Router from 'react-router-component';
import CatalogDetail from './product/app-catalogdetail';
import Template from './app-template.js';

const Locations = Router.Locations;
const Location  = Router.Location;

const App = React.createClass({
  render:function(){
    return (
      <Template>
        <Locations>
          <Location path="/" handler={Catalog} />
          <Location path="/cart" handler={Cart} />
          <Location path="/item/:item" handler={CatalogDetail} />
        </Locations>
      </Template>
    );
  }
});

module.exports = App;
