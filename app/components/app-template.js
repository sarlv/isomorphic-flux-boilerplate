import React from 'react';
import Header from './header/app-header.js';

const Template = React.createClass({
  render:function(){
    return (
      <div className="container">
        <Header />
        {this.props.children}
      </div>
    );
  }
});

module.exports = Template;
