import React from 'react';
import {Link} from 'react-router-component';
import AppStore from '../../stores/app-store.js';
import StoreWatchMixin from '../../mixins/StoreWatchMixin';

function cartTotals(){
  return AppStore.getCartTotals();
}

const CartSummary = React.createClass({
  mixins: [StoreWatchMixin(cartTotals)],
  render:function(){
    return (
      <div>
        <Link href="/cart" className="btn btn-success">
          Cart Items: {this.state.qty} / ${this.state.total}
        </Link>
      </div>
    );
  }
});

module.exports = CartSummary;
