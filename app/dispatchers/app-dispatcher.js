import {Dispatcher} from 'flux';
import assign from 'react/lib/Object.assign';

const AppDispatcher = assign(new Dispatcher(), {
  handleViewAction: function(action) {
    console.log('action', action);
    this.dispatch({
      source: 'VIEW_ACTION',
      action: action
    })
  }
});

module.exports = AppDispatcher;
