import App from './components/app';
import React from 'react';
import ReactDOM from 'react-dom';
import Css from '../assets/css/style.scss';

ReactDOM.render(<App />, document.getElementById('root'));
